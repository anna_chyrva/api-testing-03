import { expect } from "chai";

type statusCodeType = 200 | 201 | 204 | 400 | 401 | 403 | 404 | 409 | 500;

export function checkStatusCode(response, statusCode: statusCodeType) {
    expect(response.statusCode, `Status Code should be ${statusCode}`).to.equal(
        statusCode
    );
}

export function checkResponseTime(response, maxResponseTime: number = 3000) {
    expect(
        response.timings.phases.total,
        `Response time should be less than ${maxResponseTime}ms`
    ).to.be.lessThan(maxResponseTime);
}

export function checkValue<T>(
    responseValue: T,
    valueToCompare: T,
    message?: string
) {
    expect(responseValue).to.be.equal(valueToCompare, message ?? undefined);
}

export function checkDeepEquarValue<T>(
    responseValue: T,
    valueToCompare: T,
    message?: string
) {
    expect(responseValue).to.be.deep.equal(
        valueToCompare,
        message ?? undefined
    );
}
