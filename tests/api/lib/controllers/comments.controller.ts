import { ApiRequest } from "../request";
import { API_URL, POST } from "../../shared/constants";
import { ICommentData } from "../../shared/models/comments.interface";

export class CommentsController {
    async addComment(commentData: ICommentData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(POST)
            .url(`api/Comments`)
            .body(commentData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
