import { ApiRequest } from "../request";
import { API_URL, GET, PUT, DELETE } from "../../shared/constants";

export class UsersController {
    async getAllUsers() {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(GET)
            .url(`api/Users`)
            .send();
        return response;
    }

    async getUserById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(GET)
            .url(`api/Users/${id}`)
            .send();
        return response;
    }

    async getCurrentUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(GET)
            .url(`api/Users/fromToken`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async updateUser(userData: object, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(PUT)
            .url(`api/Users`)
            .body(userData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async removeUserById(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(DELETE)
            .url(`api/Users/${id}`)
            .send();
        return response;
    }
}
