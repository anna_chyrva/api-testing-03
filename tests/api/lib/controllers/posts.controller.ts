import { ApiRequest } from "../request";
import { API_URL, GET, POST } from "../../shared/constants";
import { ILikeData, IPostData } from "../../shared/models/posts.interface";

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(GET)
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPost(postData: IPostData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(POST)
            .url(`api/Posts`)
            .body(postData)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async addLikeToPost(likeData: ILikeData, accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(API_URL)
            .method(POST)
            .url(`api/Posts`)
            .body(likeData)
            .bearerToken(accessToken)
            .send();
        return response;
    }
}
