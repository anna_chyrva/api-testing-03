export interface ICommentData {
    authorId: number;
    postId: number;
    body: string;
}
