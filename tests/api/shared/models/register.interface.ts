export interface IRegisterBody {
    avatar: string;
    email: string;
    userName: string;
    password: string;
}
