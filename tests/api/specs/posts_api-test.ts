import chai, { expect } from "chai";
import chaiJsonSchema from "chai-json-schema";
import {
    checkDeepEquarValue,
    checkResponseTime,
    checkStatusCode,
    checkValue,
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { PostsController } from "../lib/controllers/posts.controller";
import { CommentsController } from "../lib/controllers/comments.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { USER_DATA } from "../shared/constants";
import { IUser } from "../shared/models/user.interface";

const auth = new AuthController();
const post = new PostsController();
const users = new UsersController();
const comment = new CommentsController();

chai.use(chaiJsonSchema);

describe("Posts Api testing", () => {
    let accessToken: string;
    let userData, postData;

    before("Login and get the token", async () => {
        const response = await auth.login(USER_DATA.email, USER_DATA.password);

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
    });

    it(`Get all posts`, async () => {
        const response = await post.getAllPosts();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(
            response.body.length,
            `Response body should have more than 1 item`
        ).to.be.greaterThan(1);
    });

    it("Get user data from the token", async () => {
        const response = await users.getCurrentUser(accessToken);

        userData = Object.assign({}, response.body);
        console.log("userData:", userData);

        checkStatusCode(response, 200);
        checkValue<string>(response.body.email, USER_DATA.email);
    });

    it(`Create new post`, async () => {
        const postBody = {
            authorId: userData.id,
            previewImage: "new_post_photo.png",
            body: "Test post information body",
        };

        const response = await post.createPost(postBody, accessToken);

        postData = Object.assign({}, response.body);

        checkStatusCode(response, 200);
        checkDeepEquarValue<IUser>(
            response.body.author,
            userData,
            "Author details isn't correct"
        );
        checkValue<string>(response.body.body, postBody.body);
        checkValue<string>(response.body.previewImage, postBody.previewImage);
    });

    it(`Add like to the post`, async () => {
        const likeBody = {
            entityId: postData.id,
            userId: userData.id,
            isLike: true,
        };

        const response = await post.addLikeToPost(likeBody, accessToken);

        checkStatusCode(response, 200);
    });

    it(`Add comment to the post`, async () => {
        const commentBody = {
            authorId: userData.id,
            postId: postData.id,
            body: "New comment for the new awesome post",
        };

        const response = await comment.addComment(commentBody, accessToken);

        checkStatusCode(response, 200);
        checkDeepEquarValue<IUser>(
            response.body.author,
            userData,
            "Author details isn't correct"
        );
        checkValue<string>(response.body.body, commentBody.body);
    });
});
