import chai, { expect } from "chai";
import chaiJsonSchema from "chai-json-schema";
import {
    checkDeepEquarValue,
    checkResponseTime,
    checkStatusCode,
    checkValue,
} from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { USER_DATA } from "../shared/constants";
import { IUser } from "../shared/models/user.interface";

const schemas = require("./data/schemas_testData.json");

const users = new UsersController();
const register = new RegisterController();
const auth = new AuthController();

chai.use(chaiJsonSchema);

describe("Happy Path", () => {
    let accessToken: string;
    let userDataBeforeUpdate;
    const NEW_USER_NAME: string = "My new user name";

    before(`Register new user`, async () => {
        const response = await register.register(USER_DATA);

        checkStatusCode(response, 201);
        checkValue<string>(response.body.user.email, USER_DATA.email);
    });

    it(`Get all users`, async () => {
        const response = await users.getAllUsers();

        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);
        expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);  
    });

    it("Login and get the token", async () => {
        const response = await auth.login(USER_DATA.email, USER_DATA.password);

        accessToken = response.body.token.accessToken.token;

        checkStatusCode(response, 200);
    });

    it("Get user data from the token", async () => {
        const response = await users.getCurrentUser(accessToken);

        userDataBeforeUpdate = Object.assign({}, response.body);

        checkStatusCode(response, 200);
        checkValue<string>(response.body.email, USER_DATA.email);
    });

    it("Update user data", async () => {
        const response = await users.updateUser(
            { ...userDataBeforeUpdate, userName: NEW_USER_NAME },
            accessToken
        );

        checkStatusCode(response, 204);
    });

    it("Get user data from the token after updating user info", async () => {
        const response = await users.getCurrentUser(accessToken);

        userDataBeforeUpdate = Object.assign({}, response.body);

        checkStatusCode(response, 200);
        checkValue<string>(response.body.userName, NEW_USER_NAME);
    });

    it(`Get user info by id after updating`, async () => {
        const response = await users.getUserById(userDataBeforeUpdate.id);

        checkStatusCode(response, 200);
        checkDeepEquarValue<IUser>(
            response.body,
            userDataBeforeUpdate,
            "User details isn't correct"
        );
    });

    after(`Remove the specific user by id`, async () => {
        const response = await users.removeUserById(userDataBeforeUpdate.id);

        checkStatusCode(response, 200);
    });
});
